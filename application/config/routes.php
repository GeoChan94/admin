<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['caracteristica/(:num)'] = 'caracteristica/index/$1';
$route['previo_has_servicio/(:num)'] = 'previo_has_servicio/index/$1';

$route['default_controller'] = 'dashboard/index';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
