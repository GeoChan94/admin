<?php
/* 
 * Generated by CRUDigniter v3.2 
 * www.crudigniter.com
 */
 
class Previo_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get previo by id_previo
     */
    function get_previo($id_previo)
    {
        return $this->db->get_where('previo',array('id_previo'=>$id_previo))->row_array();
    }
    
    /*
     * Get all previo count
     */
    function get_all_previo_count()
    {
        $this->db->from('previo');
        return $this->db->count_all_results();
    }
        
    /*
     * Get all previo
     */
    function get_all_previo($params = array())
    {
        $this->db->order_by('id_previo', 'desc');
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        return $this->db->get('previo')->result_array();
    }
        
    /*
     * function to add new previo
     */
    function add_previo($params)
    {
        $this->db->insert('previo',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update previo
     */
    function update_previo($id_previo,$params)
    {
        $this->db->where('id_previo',$id_previo);
        return $this->db->update('previo',$params);
    }
    
    /*
     * function to delete previo
     */
    function delete_previo($id_previo)
    {
        return $this->db->delete('previo',array('id_previo'=>$id_previo));
    }

    function get_all_points(){
        return $this->db->query("SELECT * FROM previo WHERE 1 = 1;")->result_array();
    }
}
