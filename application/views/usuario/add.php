<?php echo form_open('usuario/add',array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="tipo_usuario_id_tipo_usuario" class="col-md-4 control-label"><span class="text-danger">*</span>Tipo Usuario</label>
		<div class="col-md-8">
			<select name="tipo_usuario_id_tipo_usuario" class="form-control">
				<option value="">select tipo_usuario</option>
				<?php 
				foreach($all_tipo_usuario as $tipo_usuario)
				{
					$selected = ($tipo_usuario['id_tipo_usuario'] == $this->input->post('tipo_usuario_id_tipo_usuario')) ? ' selected="selected"' : "";

					echo '<option value="'.$tipo_usuario['id_tipo_usuario'].'" '.$selected.'>'.$tipo_usuario['id_tipo_usuario'].'</option>';
				} 
				?>
			</select>
			<span class="text-danger"><?php echo form_error('tipo_usuario_id_tipo_usuario');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="usuario" class="col-md-4 control-label"><span class="text-danger">*</span>Usuario</label>
		<div class="col-md-8">
			<input type="text" name="usuario" value="<?php echo $this->input->post('usuario'); ?>" class="form-control" id="usuario" />
			<span class="text-danger"><?php echo form_error('usuario');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="contrasenia" class="col-md-4 control-label"><span class="text-danger">*</span>Contrasenia</label>
		<div class="col-md-8">
			<input type="text" name="contrasenia" value="<?php echo $this->input->post('contrasenia'); ?>" class="form-control" id="contrasenia" />
			<span class="text-danger"><?php echo form_error('contrasenia');?></span>
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>

<?php echo form_close(); ?>