<nav id="main-menu" class=" navbar  navbar-expand-lg navbar-light bg-light bg-custom navbar-main" style="    box-shadow: 0px 0px 20px 1px #304e6b;">
        <div class="container">

            <a class="navbar-brand " href="<?=base_url();?>">
                <img src="http://maya.com.pe/img/logoe2.jpg" alt="" style="height: 40px;">
                <!-- <span class="h1-lato">MAYA</span> -->
                <span class="font-weight-bold text-lowercase">MAYA admin</span>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse " id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto nav ">
                    <li class="nav-item active">
                        <a data-item="" class="nav-link h1-lato " href="<?=base_url();?>"><span class="fa fa-home"></span> dashboard <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a data-item="#ofrecemos" class="nav-link h1-lato" href="<?=base_url();?>previo">previos</a>
                    </li>
                    <li class="nav-item">
                        <a data-item="#ofrecemos" class="nav-link h1-lato" href="<?=base_url();?>usuario">usuarios</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle h1-lato" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          configuracion
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                          <a class="dropdown-item" href="<?=base_url();?>tipo_usuario">tipo de usuario</a>
                          <div class="dropdown-divider"></div>
                          <a class="dropdown-item" href="<?=base_url();?>servicio">tipo de Servicios</a>
                          <div class="dropdown-divider"></div>
                          <a class="dropdown-item" href="<?=base_url();?>provincia_previo">provincias</a>
                          <div class="dropdown-divider"></div>
                          <a class="dropdown-item" href="<?=base_url();?>tipo_documento_previo">tipos de documento</a>
                          <div class="dropdown-divider"></div>
                          <a class="dropdown-item" href="<?=base_url();?>tipo_pago">tipos de pago</a>
                          <div class="dropdown-divider"></div>
                          <a class="dropdown-item" href="<?=base_url();?>tipo_previo">tipos de previo</a>
                          <div class="dropdown-divider"></div>
                          <a class="dropdown-item" href="<?=base_url();?>modalidad_previo">modalidad de previo</a>
                        </div>
                      </li>
                     <!-- <li class="nav-item">
                        <a class="nav-link btn_login" href="#"> 
                            <span class=" border text-danger  border-danger rounded pt-1 pb-1 pr-3 pl-3 h1-lato">Entrar</span>
                        </a>
                      </li> -->
      
                </ul>
            </div>
        </div>

    </nav>
