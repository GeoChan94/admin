<div class="pull-right">
	<a href="<?php echo site_url('caracteristica/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>Id Caracteristica</th>
		<th>Pisos</th>
		<th>Orientacion</th>
		<th>Estacionamientos</th>
		<th>Antiguedad</th>
		<th>Banios</th>
		<th>Dormitorios</th>
		<th>Actions</th>
    </tr>
	<?php foreach($caracteristica as $c){ ?>
    <tr>
		<td><?php echo $c['id_caracteristica']; ?></td>
		<td><?php echo $c['pisos']; ?></td>
		<td><?php echo $c['orientacion']; ?></td>
		<td><?php echo $c['estacionamientos']; ?></td>
		<td><?php echo $c['antiguedad']; ?></td>
		<td><?php echo $c['banios']; ?></td>
		<td><?php echo $c['dormitorios']; ?></td>
		<td>
            <a href="<?php echo site_url('caracteristica/edit/'.$c['id_caracteristica']); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php echo site_url('caracteristica/remove/'.$c['id_caracteristica']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>
