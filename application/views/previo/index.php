<div class="pull-right">
	<a href="<?php echo site_url('previo/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>Nº</th>
		<!-- <th>Tipo Pago Id Tipo Pago</th> -->
		<!-- <th>Tipo Previo Id Tipo Previo</th> -->

		<!-- <th>Tipo Documento Previo Id Tipo Documento Previo</th> -->
		<th>Titulo</th>
		<!-- <th>Ubicacion Geografica</th> -->
		<!-- <th>Mantenimiento</th> -->
		<th>Area</th>
		<!-- <th>Medida Frente</th> -->
		<!-- <th>Medida Fondo</th> -->
		<!-- <th>Uri Imagen Destacada</th> -->
		<th>Precio</th>
		<!-- <th>Creacion</th> -->
		<!-- <th>Descripcion Previo</th> -->
		<th>Actions</th>
    </tr>
	<?php 
	if (!empty($previo)) {
		foreach($previo as $p){ ?>

			
		<tr>
			<td><?php echo $p['id_previo']; ?></td>
			<!-- <td><?php echo $p['tipo_pago_id_tipo_pago']; ?></td> -->
			<!-- <td><?php echo $p['tipo_previo_id_tipo_previo']; ?></td> -->
			<!-- <td><?php echo $p['provincia_previo_id_provincia']; ?></td>
			<td><?php echo $p['modalidad_previo_id_modalidad_previo']; ?></td> -->
			<!-- <td><?php echo $p['tipo_documento_previo_id_tipo_documento_previo']; ?></td> -->
			<td>
				<a class="font-weight-bold text-black" href="<?php echo $p['uri_previo']; ?>" target='_blank'><?php echo $p['titulo']; ?></a><br>
				<span class="text-primary "><?php echo $p['modalidad_previo_id_modalidad_previo']; ?></span>
				<span class="text-secondary "><?php echo $p['provincia_previo_id_provincia']; ?></span>
			</td>
			<!-- <td><?php echo $p['ubicacion_geografica']; ?></td> -->
			<!-- <td><?php echo $p['mantenimiento']; ?></td> -->
			<td><?php echo $p['area']; ?></td>
			<!-- <td><?php echo $p['medida_frente']; ?></td> -->
			<!-- <td><?php echo $p['medida_fondo']; ?></td> -->
			<!-- <td><?php echo $p['uri_imagen_destacada']; ?></td> -->
			<td><?php echo $p['precio']; ?></td>
			<!-- <td><?php echo $p['creacion']; ?></td> -->
			<!-- <td><?php echo $p['descripcion_previo']; ?></td> -->
			<td>
				<a href="<?php echo site_url('previo_has_servicio/'.$p['id_previo']); ?>" class="btn btn-sm btn-warning btn-xs" title="agregar servicios" data-toggle="tooltip"><span class="fa fa-shower" ></span></a> 
				<a href="<?php echo site_url('caracteristica/'.$p['id_previo']); ?>" class="btn btn-sm btn-primary btn-xs" title="agregar caracteristicas" data-toggle="tooltip"><span class="fa fa-list"></span></a> 
				<a href="<?php echo site_url('galeria/add/'.$p['id_previo']); ?>" class="btn btn-sm btn-success btn-xs" title="agregar galeria" data-toggle="tooltip"><span class="fa fa-photo"></span></a> 
	            <a href="<?php echo site_url('previo/edit/'.$p['id_previo']); ?>" class="btn btn-sm btn-info btn-xs" title="editar previo" data-toggle="tooltip"><span class="fa fa-pencil"></span></a> 
	            <a href="<?php echo site_url('previo/remove/'.$p['id_previo']); ?>" class="btn btn-sm btn-danger btn-xs" title="eliminar previo" data-toggle="tooltip"><span class="fa fa-close"></span></a>
	        </td>
	    </tr>
		<?php } 
	}
	?>
    
</table>
<div class="pull-right">
    <?php echo $this->pagination->create_links(); ?>    
</div>
<script type="text/javascript">
	$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	})
</script>