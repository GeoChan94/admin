<!DOCTYPE html>
<html>
<head>
  <meta charset=utf-8 />
  <title></title>
  <script src='https://api.mapbox.com/mapbox.js/v3.0.1/mapbox.js'></script>
  <link href='https://api.mapbox.com/mapbox.js/v3.0.1/mapbox.css' rel='stylesheet' />
  <style>
    body {
      margin: 0;
      padding :0;
    }
    .map {
      position: absolute;
      top: 0;
      bottom: 0;
      width: 100%;
    }
  </style>
</head>
<body>
<div id='map-popups' class='map'> </div>
<script>

var geoJsonPoints = [];
(JSON.parse('<?=json_encode($points)?>')).forEach( (item, index) => {
	var arrayCoordinates = item.ubicacion_geografica.split(",");
	geoJsonPoints.push(
		{
		    type: 'Feature',
		    geometry: {
		      type: 'Point',
		      coordinates: [arrayCoordinates[0],arrayCoordinates[1]]
		    },
		    properties: {
		      title: item.titulo,
		      description: item.descripcion_previo,
		      'marker-color': '#CD5C5C',
		      'marker-size': 'medium', // small, medium, large
		    }
		}
	);
});

L.mapbox.accessToken = 'pk.eyJ1IjoiaXZhbjcyMzkxIiwiYSI6ImNpb2xqZG1oOTAxcW11eG0xYzU2djlyY3YifQ.eR7IsqzlP6I6-h9lBDGfsw';
var mapPopups = L.mapbox.map('map-popups', 'mapbox.light')
  .setView([-15.8405466,-70.0280772], 13);
var myLayer = L.mapbox.featureLayer().addTo(mapPopups);

myLayer.setGeoJSON(geoJsonPoints);
mapPopups.scrollWheelZoom.disable();
</script>
</body>
</html>