<?php
/* 
 * Generated by CRUDigniter v3.2 
 * www.crudigniter.com
 */
 
class Modalidad_previo extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Modalidad_previo_model');
    } 

    /*
     * Listing of modalidad_previo
     */
    function index()
    {
        $data['modalidad_previo'] = $this->Modalidad_previo_model->get_all_modalidad_previo();
        
        $data['_view'] = 'modalidad_previo/index';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Adding a new modalidad_previo
     */
    function add()
    {   
        $this->load->library('form_validation');

		$this->form_validation->set_rules('nombre','Nombre','required|max_length[100]');
		// $this->form_validation->set_rules('descripcion','Descripcion','required|max_length[200]');
		
		if($this->form_validation->run())     
        {   
            $params = array(
				'nombre' => $this->input->post('nombre'),
				'descripcion' => $this->input->post('descripcion'),
            );
            
            $modalidad_previo_id = $this->Modalidad_previo_model->add_modalidad_previo($params);
            redirect('modalidad_previo/index');
        }
        else
        {            
            $data['_view'] = 'modalidad_previo/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a modalidad_previo
     */
    function edit($id_modalidad_previo)
    {   
        // check if the modalidad_previo exists before trying to edit it
        $data['modalidad_previo'] = $this->Modalidad_previo_model->get_modalidad_previo($id_modalidad_previo);
        
        if(isset($data['modalidad_previo']['id_modalidad_previo']))
        {
            $this->load->library('form_validation');

			$this->form_validation->set_rules('nombre','Nombre','required|max_length[100]');
			$this->form_validation->set_rules('descripcion','Descripcion','required|max_length[200]');
		
			if($this->form_validation->run())     
            {   
                $params = array(
					'nombre' => $this->input->post('nombre'),
					'descripcion' => $this->input->post('descripcion'),
                );

                $this->Modalidad_previo_model->update_modalidad_previo($id_modalidad_previo,$params);            
                redirect('modalidad_previo/index');
            }
            else
            {
                $data['_view'] = 'modalidad_previo/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The modalidad_previo you are trying to edit does not exist.');
    } 

    /*
     * Deleting modalidad_previo
     */
    function remove($id_modalidad_previo)
    {
        $modalidad_previo = $this->Modalidad_previo_model->get_modalidad_previo($id_modalidad_previo);

        // check if the modalidad_previo exists before trying to delete it
        if(isset($modalidad_previo['id_modalidad_previo']))
        {
            $this->Modalidad_previo_model->delete_modalidad_previo($id_modalidad_previo);
            redirect('modalidad_previo/index');
        }
        else
            show_error('The modalidad_previo you are trying to delete does not exist.');
    }
    
}
