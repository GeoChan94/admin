<?php

class Galeria extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Galerium_model');
        $this->load->model('Previo_model');
        $this->load->model('Previo_has_galerium_model');
    } 

    /*
     * Listing of galeria
     */
    function index()
    {
        $data['galeria'] = $this->Galerium_model->get_all_galeria();
        
        $data['_view'] = 'galerium/index';
        $this->load->view('layouts/main',$data);
    }

    function add( $id_predio = null ){


        $this->load->library('form_validation');

		$this->form_validation->set_rules('type','Type','required');
		
		if($this->form_validation->run()){   
            
            if ( $this->input->post('btn-submit') === "guardar" ) {
                foreach ($_POST['imagen'] as $key => $value) {
                    $params = array(
                        //'id_galeria' => (Integer)date('His'), // Comentar cuando la id_galeria se correja a AUTO_INCREMENT
                        /*
                        'type' => $this->input->post('type'),
                        'uri' => $this->input->post('uri'),
                        'title' => $this->input->post('title'),
                        */
                        'tipo'          => $_POST['tipo'][$key],
                        'uri_galeria'   => $value,
                        //'title' => $_POST['titulo'][$key],
                        //'date_upload' => date('Y-m-d H:i:s'),
                    );
                    $galeria = $this->Galerium_model->get_galeriumExist($id_predio,$value);
                    if ( !@$galeria ) { // Si no existe, insertamos en la base de datos.
                        $galerium_id = $this->Galerium_model->add_galerium($params);
                        $paramsPredioHasGaleria = array(
                            //'id_previo_has_galeriacol'  => date('His'),
                            'previo_id_previo'          => $id_predio,
                            'galeria_id_galeria'        => $galerium_id,
                        );
                        $predio_has_galeria_id = $this->Previo_has_galerium_model->add_previo_has_galerium($paramsPredioHasGaleria);
                    }
                }
            }else{ // Si no guarda el formulario se elimina las fotos cargadas
                foreach ($_POST['imagen'] as $key => $value) {
                    $this->eliminarArchivo($value);
                }
            }
            
            //$galerium_id = $this->Galerium_model->add_galerium($params);
            redirect('previo');
        }else{            
            $data['predio'] = $this->Previo_model->get_previo($id_predio);
            $data['archivos'] = $this->Galerium_model->GaleriaByPredio($id_predio);
            $data['_view'] = 'galerium/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a galerium
     */
    function edit($id_galeria)
    {   
        // check if the galerium exists before trying to edit it
        $data['galerium'] = $this->Galerium_model->get_galerium($id_galeria);
        
        if(isset($data['galerium']['id_galeria']))
        {
            $this->load->library('form_validation');

			$this->form_validation->set_rules('uri_galeria','Uri Galeria','required|max_length[300]');
		
			if($this->form_validation->run())     
            {   
                $params = array(
					'uri_galeria' => $this->input->post('uri_galeria'),
                );

                $this->Galerium_model->update_galerium($id_galeria,$params);            
                redirect('galerium/index');
            }
            else
            {
                $data['_view'] = 'galerium/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The galerium you are trying to edit does not exist.');
    } 


    function remove($id_galeria = null, $uri = null, $option = null ){
        $data = null;
        if (!$id_galeria) {
            $id_galeria = $this->input->post('id');
            $option     = $this->input->post('option');
            $uri        = $this->input->post('uri');
        }

        if ( (Integer)$option === 1 ) { // $option === 1; Eliminación simple de la carpeta assets/galeria/imagenes
            $result = $this->eliminarArchivo( $uri );
            $data['response'] = "success";
            $data['message'] = "Archivo eliminado correctamente..!";
        }else{ // $option === 2; Eliminar archivo y eliminar registro de la base de datos
            $galerium = $this->Galerium_model->get_galerium($id_galeria);
            // check if the galerium exists before trying to delete it
            if(isset($galerium['id_galeria'])){
                $this->Galerium_model->delete_galerium($id_galeria);
                //redirect('galerium/index');
                $this->eliminarArchivo( $uri );
                $data['response'] = "success";
                $data['message'] = "Archivo eliminado correctamente..!";
                
            }else{
                $this->eliminarArchivo( $uri );
                $data['response'] = "error";
                $data['message'] = "El archivo que intentas eliminar no existe..!";
            }
        }
        echo json_encode($data);
    }
    
    function eliminarArchivo($uri = null ){
        $uri_patch = './assets/galeria/imagenes/'.trim($uri);
        if(!unlink( $uri_patch ) ){
            return false;
        }else{
            return true;
        }
    }
}
